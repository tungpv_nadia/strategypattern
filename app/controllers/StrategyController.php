<?php
/**
 * 
 */
use Strategy\Car;
use Strategy\StrategyContext;
use Strategy\StrategyA;
use Strategy\StrategyB;
use Strategy\StrategyC;


 class StrategyController extends BaseController
 {
 	public function testStrategy(){

 		$car = new Car('Ducati', 'red', '2014');

 		$strategyA = new StrategyA(1);
 		// $strategyB = new StrategyB(2);
 		// $strategyC = new StrategyC(3);

 	// 	$result = array
 	// 	(
 	// 		'strategyA' => $strategyA->showMaker($car),
 	// 		'strategyB' => $strategyB->showColor($car),
 	// 		'strategyC' => $strategyC->showYear($car)
 	// 	);
 	// 	return View::make('/show', compact('result'));
 	// }

 		$result = array
 		(
 			'strategyA' => $strategyA,
 			// 'strategyB' => $strategyB,
 			// 'strategyC' => $strategyC
 		);
 		return View::make('/show', compact('result', 'car'));
 	}
 } 
 ?>