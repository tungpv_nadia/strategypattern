<?php
	/**
	 * interface 
	 */
	namespace Strategy;

	interface StrategyInterface
	{
		public function showMaker($car);
		public function showColor($car);
		public function showYear($car);
	} 
?>