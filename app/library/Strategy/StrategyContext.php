<?php
	/**
	 * 
	 */
	namespace Strategy;

	use Strategy\StrategyA;
	use Strategy\StrategyB;
	use Strategy\StrategyC;


	class StrategyContext
	{
		public $strategy;
		public function __construct($strategy)
		{
			switch ($strategy) {
				case 1:
					$this->strategy = new StrategyA();
					break;
				case 2:
					$this->strategy = new StrategyB();
					break;
				case 3:
					$this->strategy = new StrategyC();
					break;
			}
		}

		public function showMaker($car){
			return $this->strategy->showMaker($car);
		}
		public function showColor($car){
			return $this->strategy->showColor($car);
		}
		public function showYear($car){
			return $this->strategy->showYear($car);
		}
	} 
	?>