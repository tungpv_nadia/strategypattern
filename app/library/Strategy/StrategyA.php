<?php
	/**
	 * Startegy A 
	 */
	namespace Strategy;
	use Strategy\Car;
	use Strategy\StrategyInterface;

	 class StrategyA implements StrategyInterface
	 {
	 	public function showMaker($car){
	 		$maker = $car->getMaker();
	 		return $maker;
	 	}
	 	public function showColor($car){
	 		$color = $car->getColor();
	 		return $color;
	 	}
	 	public function showYear($car){
	 		$year = $car->getYear();
	 		return $year;
	 	}
	 } 
 ?>