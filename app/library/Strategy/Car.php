<?php
/**
 * Car object 
 */
namespace Strategy;

	class Car
	{
		private $maker;
		private $color;
		private $year;

		public function __construct($maker, $color, $year){
			$this->maker = $maker;
			$this->color = $color;
			$this->year = $year;
		}

		public function setMaker()
		{
			$this->maker = 'maker';
		}
		public function setColor($color){

			$this->color = $color;
		}
		public function setYear($year)
		{
			$this->year = $year;
		}
		public function getMaker()
		{
			return $this->maker;
		}
		public function getColor()
		{
			return $this->color;
		}
		public function getYear()
		{
			return $this->year;
		}
		public function getAll(){
			return $this->getMaker().'-'.$this->getColor().'-'.$this->getYear();
		}
	} 
?>